.deg2rad <- function(deg=90) {
  rad <- (deg * pi) / 180
  return(rad)
}

.rotate.xyz <- function(xyz, deg=90) {
  deg <- .deg2rad(deg)
  x.inds <- seq(1, length(xyz), by=3)
  y.inds <- seq(2, length(xyz), by=3)
  z.inds <- seq(3, length(xyz), by=3)
  
  P2.x = xyz[x.inds] * cos(deg) - xyz[y.inds] * sin(deg)
  P2.y = xyz[x.inds] * sin(deg) + xyz[y.inds] * cos(deg)

  xyz[x.inds]=P2.x
  xyz[y.inds]=P2.y
  return(xyz)
}

draw.sdfs <- function(sdfs, inds=NULL, ...) {
  if(!is.sdfs(sdfs))
    stop("provide a 'sdfs' object as obtained from function 'read.sdf'")

  if(!is.null(inds)) {
    new <- list()
    for(i in 1:length(inds)) {
      new[[i]] <- sdfs[[inds[i]]]
    }
    sdfs <- new
  }
  
  xyz <- NULL
  for ( i in 1:length(sdfs) )
    xyz <- rbind(xyz, matrix(sdfs[[i]]$xyz, ncol=3, byrow=T))

  xlim=range(xyz[,1])
  ylim=range(xyz[,2])

  for(i in 1:length(sdfs)) {
    tmp <- sdfs[[i]]
  
    draw.sdf(tmp, ...) ##, xlim=xlim, ylim=ylim)
    ##mtext(tmp$name, side=3, line=0, adj=0)
  }
  
  invisible(list(xlim=xlim, ylim=ylim))
}


draw.sdf <- function(sdf, hydrogens=FALSE,
                     labels=NULL, molname=TRUE,
                     gen2d=FALSE, remake=FALSE, orient=FALSE,
                     xlim=NULL, ylim=NULL, verbose=FALSE,
                     lty=1, lwd=1, col=1, cex=1, ...) {
  
  if(!is.sdf(sdf))
    stop("input should be of class 'sdf' as obtained by 'read.sdf'")
  
  atom.numbers <- seq(1, nrow(sdf$atom))

  if(length((which(c(gen2d, remake, orient))))>1) {
    stop("incompatible arguments. use either 
          'gen2d=TRUE', 'remake=TRUE', or 'orient=TRUE'")
  }

  if(!hydrogens) {
    if(any(sdf$atom[,"elety"] == "H")) {
      inds.noh <- atom.select.sdf(sdf, "noh", verbose=verbose)
      sdf <- trim.sdf(sdf, inds.noh)
      atom.numbers <- atom.numbers[inds.noh$atom]
    }
  }
  
  if(orient) {
    sdf$xyz  <- orient.sdf(sdf)
    sdf$atom[,c("x", "y", "z")] <- matrix(as.character(sdf$xyz), ncol=3, byrow=T)
  }
  
  if(remake) {
    if(gen2d | hydrogens) {
      warning("incompatible arguments. forcing 'gen2d=FALSE' and 'hydrogens=FALSE' when 'remake=TRUE'")
      hydrogens <- FALSE
      gen2d <- FALSE
    }
    sdf <- trim.sdf(sdf, atom.select(sdf, "noh", verbose=verbose))
    sdf <- as.sdf(as.smiles(sdf))
  }
  
  if(gen2d) {
    tmpf1 <- tempfile(fileext=".sdf")
    tmpf2 <- tempfile(fileext=".sdf")
    write.sdf(sdf, file=tmpf1)
    
    cmd <- paste("--gen2D -isdf", tmpf1, "-osdf", tmpf2, sep=" ")
    success <- .runBabel(cmd, verbose=verbose, ...)
    
    sdf <- read.sdf(tmpf2)
    unlink(tmpf1)
    unlink(tmpf2)
  }
  
  if(is.logical(labels)) {
    if(labels)
      labels <- "eleno"
    else
      labels <- NULL
  }
  
  if(!is.null(labels)) {
    if(length(labels)>1) {
      if(length(labels)!=nrow(sdf$atom))
        stop("length of 'labels' does not match number of atoms in sdf")
    }
    else if("eleno" %in% labels) {
      labels <- as.character(atom.numbers)
      labels <- paste(sdf$atom[,"elety"], labels, sep="")
    }
    else {
      labels <- NULL
      warning("provide 'labels' as vector of length equal to number of atoms, or set labels='eleno'")
    }
  }

  if(length(col) > 1) {
    if(length(col) != nrow(sdf$bond))
      stop("'col' must be of length one or equal to the number of bonds")
  }

  if(length(lty) > 1) {
    if(length(lty) != nrow(sdf$bond))
      stop("'lty' must be of length one or equal to the number of bonds")
  }

  if(length(lwd) > 1) {
    if(length(lwd) != nrow(sdf$bond))
      stop("'lwd' must be of length one or equal to the number of bonds")
  }

  
  xyz <- matrix(sdf$xyz, ncol=3, byrow=T)
  colnames(xyz) <- c("x", "y", "z")
  
  cols <- rep(NA, length(sdf$atom[,"elety"]))
  cols[ sdf$atom[,"elety"] == "O" ] = 2
  cols[ sdf$atom[,"elety"] == "N" ] = 4
  cols[ sdf$atom[,"elety"] == "S" ] = "orange"
  cols[ sdf$atom[,"elety"] == "H" ] = "grey50"
  ##cols[ sdf$atom[,"elety"] == "Cl" ] = 3
  ##cols[ sdf$atom[,"elety"] == "Br" ] = 3
  ##cols[ sdf$atom[,"elety"] == "F" ] = 3

  ## other non O, N, S, H, C
  other_atoms <- !sdf$atom[,"elety"] %in% c("O", "N", "S", "H", "C")
  cols[ other_atoms ] <- 3

  ## R/X-groups
  r.inds <- c(grep("R", sdf$atom$elety), grep("X", sdf$atom$elety))
  if(length(r.inds)>0)
    cols[ r.inds ] = 5

  ## find rings
  rdata <- .find.rings(sdf)
  if(!is.null(rdata$rings))
    ring.atoms <- unique(sort(unlist(rdata$rings)))
  else
    ring.atoms <- NULL

  ## check for aromatic rings
  #if(any(sdf$bond$type == 4)) {
  #  for(j in 1:length(rdata$bonds)) {
  #    inds = rdata$bonds[[j]]
  #    
  #    if(all(sdf$bond$type[ inds ] == 4)) {
  #      sdf$bond$type[ inds[seq(1, length(inds), by=2)] ] = 1
  #      sdf$bond$type[ inds[seq(2, length(inds), by=2)] ] = 2
  #    }
  #  }
  #}
  
  ## initialize plot
  if(is.null(xlim))
    xlim <- range(xyz[,"x"])
  if(is.null(ylim))
    ylim <- range(xyz[,"y"])
  
  plot.new()
  plot.window(xlim=xlim,
              ylim=ylim,
              asp=1)

  
  for(i in 1:nrow(sdf$bond) ) {
    a <- sdf$bond$a[i]
    b <- sdf$bond$b[i]
    bondtype <- sdf$bond[i,"type"]
      
    if(!is.null(ring.atoms))
      ring <- ((a %in% ring.atoms) & (b %in% ring.atoms)) & (i %in% rdata$backedges)
    else
      ring <- FALSE

    if(sdf$atom[a, "elety"]=="H" | sdf$atom[b, "elety"]=="H") {
      bondtype <- 1
      lty1 <- 6
      col1 <- "grey50"
    }
    else {

      if(length(col) > 1)
        col1 <- col[i]
      else
        col1 <- col
      
      if(length(lty) > 1)
        lty1 <- lty[i]
      else
        lty1 <- lty

      if(length(lwd) > 1)
        lwd1 <- lwd[i]
      else
        lwd1 <- lwd
      
    }

    ## bond type meaning
    ## 1- single
    ## 2- double
    ## 3- triple
    ## 4- aromatic
    
    if(bondtype==1 || bondtype==4) {
      lines(x=c(xyz[a, "x"], xyz[b, "x"]),
            y=c(xyz[a, "y"], xyz[b, "y"]),
            lty=lty1, lwd=lwd1, col=col1)
    }
    
    if(bondtype==2 & ring) {
      ring.inds <- unlist(lapply(rdata$rings, function(x,a,b) all(c(a,b) %in% x), a,b))
      ring.ind <- 1

      ringlen <- 999; 
      for(k in 1:length(which(ring.inds))) {
        alt <- length(rdata$rings[[which(ring.inds)[k]]])
        if(alt < ringlen) {
          ring.ind <- which(ring.inds)[k]
          ringlen <- alt
        }
      }
      
      atom.inds <- rdata$rings[[ring.ind]]
      
      ## center crds for the ring
      tmp.crds <- xyz[atom.inds, c("x", "y")]
      com.a <- colSums(tmp.crds)/sum(rep(1, nrow(tmp.crds)))
      ##points(com.a[1], com.a[2], pch=16, col=2)
      
      ## center crds from 2 atoms
      tmp.crds <- rbind(xyz[a, c("x", "y")],
                        xyz[b, c("x", "y")])
      com.b <- colSums(tmp.crds)/sum(rep(1, nrow(tmp.crds)))
      ##points(com.b[1], com.b[2], pch=16, col=3)
      
      v <- normalize.vector(c(com.a[1] - com.b[1],
                              com.a[2] - com.b[2]))*0.15
      wa <- normalize.vector(c(xyz[a, "x"] - com.b[1],
                               xyz[a, "y"] - com.b[2]))*0.15
      wb <- normalize.vector(c(xyz[b, "x"] - com.b[1],
                               xyz[b, "y"] - com.b[2]))*0.15
      p <- v


      lines(x=c(xyz[a, "x"], xyz[b, "x"]),
            y=c(xyz[a, "y"], xyz[b, "y"]),
            lty=lty1, lwd=lwd1, col=col1)
      
      lines(x=c(xyz[a, "x"] + p[1] -wa[1], xyz[b, "x"] + p[1]-wb[1]),
            y=c(xyz[a, "y"] + p[2] -wa[2], xyz[b, "y"] + p[2]-wb[2]),
            lty=1, col="grey50", lwd=lwd1)
    }

    if(bondtype==2 & !ring) {
      v <- normalize.vector(c(xyz[b, "x"] - xyz[a, "x"],
                                xyz[b, "y"] - xyz[a, "y"]))
      p <- c(-v[2], v[1])*0.0725
      wa <- c(0,0)
      wb <- wa

      lines(x=c(xyz[a, "x"] + p[1], xyz[b, "x"] + p[1]),
            y=c(xyz[a, "y"] + p[2], xyz[b, "y"] + p[2]),
            lty=lty1, lwd=lwd1, col=col1)
      
      lines(x=c(xyz[a, "x"] - p[1], xyz[b, "x"] - p[1]),
            y=c(xyz[a, "y"] - p[2], xyz[b, "y"] - p[2]),
            lty=lty1, lwd=lwd1, col=col1)

    }
    
    
    if(bondtype==3) {
      v <- normalize.vector(c(xyz[b, "x"] - xyz[a, "x"],
                              xyz[b, "y"] - xyz[a, "y"]))
      p <- c(-v[2], v[1])*0.0725
      
      lines(x=c(xyz[a, "x"], xyz[b, "x"]),
            y=c(xyz[a, "y"], xyz[b, "y"]),
            lty=lty1, lwd=lwd1, col=col1)
      
      lines(x=c(xyz[a, "x"] - p[1], xyz[b, "x"] - p[1]),
            y=c(xyz[a, "y"] - p[2], xyz[b, "y"] - p[2] ),
            lty=lty1, lwd=lwd1, col=col1)

      lines(x=c(xyz[a, "x"] + p[1], xyz[b, "x"] + p[1]),
            y=c(xyz[a, "y"] + p[2], xyz[b, "y"] + p[2] ),
            lty=lty1, lwd=lwd1, col=col1)
      
    }
  
  }

  
  
  ## draw aromatic rings (bondtype == 4)
  if(any(sdf$bond$type == 4)) {
    for(j in 1:length(rdata$bonds)) {
      inds = rdata$bonds[[j]]
      
      if(all(sdf$bond$type[ inds ] == 4)) {
        
        ## find atoms in this ring
        atoms = rdata$rings[[j]]
        cent <- com.xyz(sdf$xyz[ atom2xyz(atoms) ])
        
        rad1 = cent - sdf$xyz[ atom2xyz(atoms[1]) ]
        rad2 = sqrt( sum(rad1**2) )
        
        plotrix::draw.circle(x=cent[1], y=cent[2],
                             radius=rad2 *0.7)
        
      }
    }
  }
  

  h.inds <- NULL
  inds <- seq(1, nrow(sdf$atom))
  if(hydrogens & any(sdf$atom[, "elety"]=="H")) {
    h.inds <- which(sdf$atom[, "elety"]=="H")
    inds <- which(!(inds %in% h.inds))
  }

  pch <- 16
  if(is.null(labels)) {
    col.inds <- which(!is.na(cols))
    if(length(col.inds)>0) {
      points(xyz[col.inds,"x"], xyz[col.inds,"y"], pch=pch, col="white", cex=cex*3.5)
      text(xyz[col.inds,"x"], xyz[col.inds,"y"], labels=sdf$atom[col.inds,"elety"],
           col=cols[col.inds], cex=cex, offset=0)
    }
  }
  
  if(hydrogens & !is.null(h.inds) & is.null(labels)) {
    points(xyz[h.inds,"x"], xyz[h.inds,"y"], pch=pch, col="white", cex=cex*3.5)
    text(xyz[h.inds,"x"], xyz[h.inds,"y"], labels=sdf$atom[h.inds,"elety"],
         col="grey50", cex=0.8*cex, offset=0)
  }
  
  if(!is.null(labels)) {
    points(xyz[,"x"], xyz[,"y"], pch=pch, col="white", cex=cex*3.5)
    text(xyz[,"x"], xyz[,"y"], labels=labels, col=cols, cex=0.75*cex, offset=0)
  }

  if(molname)
    mtext(sdf$name, side=3, line=0, adj=0, cex=cex)
  
  invisible(list(xlim=xlim, ylim=ylim))
}



#add_bonds <- function(sdf, bonds=NULL, col="red", lty=1, lwd=2) {
#  xyz <- matrix(sdf$xyz, ncol=3, byrow=T)
#  colnames(xyz) <- c("x", "y", "z")

#  for(i in 1:length(bonds)) {
#    a <- sdf$bond$a[bonds[i]]
#    b <- sdf$bond$b[bonds[i]]
    
#    lines(x=c(xyz[a, "x"], xyz[b, "x"]),
#          y=c(xyz[a, "y"], xyz[b, "y"]),
#          lty=lty, col=col, lwd=lwd)
#  }  
#}
