
lig2pdb <- function(id, verbose=TRUE) {
  url <- paste('http://www.ebi.ac.uk/pdbe-srv/pdbechem/PDBEntry/download/', id, sep="")
  
  tf <- tempfile()
  download.file(url, tf, quiet = !verbose)
  
  lines <- readLines(tf, warn=!verbose)
  pdb.ids <- lines[-c(1,2)]
  
  return(pdb.ids)
}
