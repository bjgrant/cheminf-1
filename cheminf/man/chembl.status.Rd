\name{chembl.status}
\alias{chembl.status}
\title{
  Determine Status of ChEMBL REST API
}
\description{
Use this function to ensure that the ChEMBL REST API is up and running. Useful in debugging
scenarios to ascertain whether the problem is on the user or the service side.
}
\usage{
chembl.status()
}
\value{
  \code{TRUE} if the service is running, \code{FALSE} otherwise
}
\author{Rajarshi Guha}
\keyword{ chembl }
