\name{atom.select.sdf}
\alias{atom.select.sdf}
\title{ Atom Selection From SDF Structure }
\description{
  Return the atom, bond and xyz coordinates indices of a \sQuote{sdf} structure
  object corresponding to the intersection of a hierarchical selection.
}
\usage{
\method{atom.select}{sdf}(sdf, string=NULL, eleno=NULL, elety=NULL,
        inverse = FALSE, verbose=FALSE, ...)
}
\arguments{
  \item{sdf}{ a structure object of class \code{"sdf"}, obtained from
    \code{\link{read.sdf}}. }
  \item{string}{ a character selection string: \code{h} \code{noh} }
  \item{eleno}{ a numeric or character vector of element numbers. }
  \item{elety}{ a character vector of atom types / names. }
  \item{inverse}{ logical, if TRUE the inversed selection is retured
    (i.e. all atoms NOT in the selection). }
  \item{verbose}{ logical, if TRUE details of the selection are printed. }
  \item{\dots}{ arguments going nowhere. }
}
\details{
  This function allows for the selection of atom and coordinate data
  corresponding to the intersection of input criteria (such as a
  \sQuote{elena} etc.  Or a hierarchical
  \sQuote{selection string} \code{string} with a strict format.

  This function is largely a wrapper for the bio3d function
  \code{atom.select.pdb}. 
}
\value{
  Returns a list of class \code{"select"} with components:
  \item{atom }{atom indices of selected atoms.}
  \item{xyz }{xyz indices of selected atoms.}
  \item{bond }{bond indices of selected atoms.}
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
}
\author{ Lars Skjaerven }
\seealso{ 
  \code{\link{read.sdf}}, 
  \code{\link{write.sdf}}, \code{\link{read.dcd}} 
}
\examples{
f    <- system.file("examples/aspirin-3d.sdf", package="cheminf")
sdf  <- read.sdf(f)
sele <- atom.select(sdf, "noh")
}
\keyword{ sdf }