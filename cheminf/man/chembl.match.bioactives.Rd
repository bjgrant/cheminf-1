\name{chembl.match.bioactives}
\alias{chembl.match.bioactives}
\title{
  Matching bioactivty data
}
\usage{
chembl.match.bioactives(smiles=NULL, chemblid=NULL, target.id=NULL,
            match.str=NULL, sim=90, verbose=TRUE)
}
\arguments{
  \item{smiles}{ a character string of the SMILES. }
  \item{chemblid}{ a character string of the ChEMBL ID. }
  \item{target.id}{ ChEMBL ID. }
  \item{match.str}{  a character string to match }
  \item{sim}{ similarity cutoff. }
  \item{verbose}{ logical, if TRUE some additional information is printed. }
}
\description{
  Basic functionality to retrieve a compound entry from the ChEMBL database.
}
\author{
  Rajarshi Guha,
  Lars Skjaerven
}
\value{
  returns a list with the following named fields..
}
\seealso{
  \code{\link{chembl.compound}},
  \code{\link{chembl.activity}}
}
\keyword{ chembl }