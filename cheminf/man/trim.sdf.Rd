\name{trim.sdf}
\alias{trim.sdf}
\title{ Trim a SDF Object To A Subset of Atoms. }
\description{
  Produce a new smaller SDF object, containing a subset of atoms, from a
  given larger SDF object. 
}
\usage{
\method{trim}{sdf}(sdf, inds = NULL, \dots)
}
\arguments{
  \item{sdf}{ a SDF structure object obtained from
    \code{\link{read.sdf}}. }
  \item{inds}{ a list object of ATOM, BOND, and XYZ indices as obtained from
    \code{\link{atom.select.sdf}}. }
  \item{\dots}{ arguments passed to and from functions. }
}
\details{
  This is a basic utility function for creating a new SDF object based
  on a selection of atoms.
}
\value{
  Returns a \sQuote{SDF} object.
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
}
\author{ Lars Skjaerven }
\seealso{
  \code{\link{read.sdf}},
  \code{\link{atom.select}},
  \code{\link{write.sdf}},
  \code{\link{trim.pdb}},
}
\examples{
f    <- system.file("examples/aspirin-3d.sdf", package="cheminf")
sdf  <- read.sdf(f)
sele <- atom.select.sdf(sdf, "noh")
new  <- trim.sdf(sdf, sele)
}
\keyword{ sdf }
