\name{is.sdf}
\alias{is.sdf}
\alias{is.sdfs}
\alias{is.rawsdf}
\title{ Is an Object of Class \sQuote{sdf}?  }
\description{
  Checks whether its argument is an object of class \sQuote{sdf} or \sQuote{sdfs}.
}
\usage{
  is.sdf(x)
  is.sdfs(x)
  is.rawsdf(x)
}
\arguments{
  \item{x}{ an R object. }
}
\details{
  Tests if the object \sQuote{x} is of class \sQuote{sdf}
  (\code{is.sdf}), i.e. if \sQuote{x} has a
  \dQuote{class} attribute equal to \code{sdf}.
}
\value{
  TRUE if x is an object of class \sQuote{sdf} and FALSE otherwise
}
\seealso{
  \code{\link{read.sdf}}, \code{\link{as.sdf}}
}
\keyword{ classes }
