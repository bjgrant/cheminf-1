chembl.report <- function(ids, file=NULL) {
  if(is.null(file))
    stop("provide a file name for the PDF")
  
  pdf(file, width = 11.69, height = 8.27)
  layout(matrix(c(1,2,3,3), ncol=2), widths=c(1,1))
  par(mar=c(3,2,3,2), oma=c(3,3,3,3))
  
  for(i in 1:length(ids)) {
    id <- ids[i]
    comp <- chembl.compound(id)
    zincid <- chembl2zinc(id)
    
    act <- chembl.bioactives(id)
    lims <- draw.smiles(comp$smiles)
    mtext(id, side=3, line=0, adj=0, cex=1.2, at=lims$xlim[1])
    
    ymax <- 20
    plot.new()
    plot.window(xlim=c(0,ymax*2),
                ylim=c(0,ymax))
    k <- 0
    if(!is.null(name <- comp$preferredCompoundName)) {
      text(0, ymax, labels="Name:", adj=0, cex=1)
      text(12, ymax, labels=comp$preferredCompoundName, adj=0, cex=1)
      k <- 2
    }
        
    text(0, ymax-k, labels="ChEMBL ID:", adj=0, cex=1)
    text(12, ymax-k, labels=id, adj=0, cex=1)
    k <- k+2
    
    text(0, ymax-k, labels="ZINC ID:", adj=0)
    text(12, ymax-k, labels=zincid, adj=0)
    k <- k+2
    
    text(0, ymax-k, labels="SMILES:", adj=0)
    text(12, ymax-k, labels=comp$smiles, adj=0)
    k <- k+2
    
    text(0, ymax-k, labels="Known drug:", adj=0)
    text(12, ymax-k, labels=comp$knownDrug, adj=0)
    k <- k+2
    
    text(0, ymax-k, labels="Ro3:", adj=0)
    text(12, ymax-k, labels=comp$passesRuleOfThree, adj=0)
    k <- k+2

    text(0, ymax-k, labels="#Ro5 Violations:", adj=0)
    text(12, ymax-k, labels=comp$numRo5Violations, adj=0)
    k <- k+2
    
    text(0, ymax-k, labels="Med chem friendly:", adj=0)
    text(12, ymax-k, labels=comp$medChemFriendly, adj=0)

    url1 <- paste("https://www.ebi.ac.uk/chembl/compound/inspect/", id, sep="")
    url2 <- paste("http://zinc.docking.org/substance/", unlist(strsplit(zincid, "ZINC"))[2], sep="")
    mtext(url1, side=1, line=0, adj=0, cex=0.8, at=0)
    mtext(url2, side=1, line=1, adj=0, cex=0.8, at=0)

    
  
    ymax <- 40
    plot.new()
    plot.window(xlim=c(0,ymax),
                ylim=c(0,ymax))
    
    j <- 0
    text(0, ymax-j+1, labels="Bioactivity:", adj=0)
    for(i in 1:length(act$target__chemblid)) {

      if(i<8) {
        text(0, ymax-j, labels=act$target__chemblid[i], adj=0, cex=0.8)
        text(15, ymax-j, labels=act$reference[i], adj=0, cex=0.8)
        text(15, ymax-j-1, labels=act$target__name[i], adj=0, cex=0.8)
        text(15, ymax-j-2, labels=act$organism[i], adj=0, cex=0.8)
        
        txt <- paste(act$bioactivity__type[i], act$operator[i], 
                     act$value[i], act$units[i])
        text(15, ymax-j-3, labels=txt, adj=0, cex=0.8)
        
        j <- j+5
      }
      else {
        txt <- paste(length(act$target__chemblid)-7, "additional bioactivity record(s)!")
        text(0, ymax-j, labels=txt, adj=0, cex=0.8, col="red")
        break;
      }
    }

  }
    dev.off()
  
  
}
