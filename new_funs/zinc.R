#.inzinc <- function(id) {
#  uni <- unichem.compound(id)
#  if(!any(uni$src_id==9))
#    return(NULL)
#  else
#    return(uni$src_compound_id[uni$src_id==9])
#}



zinc <- function(zincid, verbose=TRUE) {
  if(length(zincid)>1) {
    zincid <- zincid[1]
    warning("multiple input ZINC ids detected. using only the first id")
  }

  url <- paste("http://zinc.docking.org/substance/", zincid, sep="")
  if(!url.exists(url))
    stop(paste("url", url, "does not exist"))
  
  
  url <- "http://zinc.docking.org/results/"
  curl.opts <- list(verbose = verbose)

  data <- RCurl::postForm(url, zinc.ids=zincid, page.format="800-lb-gorilla",
                          style = 'HTTPPOST', .opts = curl.opts,
                          .contentEncodeFun=URLencode)

  attr <- attributes(data)
  ftype <- as.character(attr$`Content-Type`)
  
  tmpf <- tempfile()
  writeBin(as.vector(data), tmpf)
  
  lines <- readLines(tmpf)
  unlink(tmpf)

  if(!length(lines)>1)
    stop("can not retrieve data from zinc")

  headers <- unlist(strsplit(lines[1], "\t"))
  Encoding(lines[2]) <- "UTF-8"
  data <- unlist(strsplit(lines[2], "\t"))
  data[data==""] <- NA


  splitter <- function(tmp, split1=";", split2=":") {
    tmp <- strsplit(unlist(strsplit(tmp, split1)), split2)
    tmp2 <- NULL
    for(i in 1:length(tmp)) {
      tmp2 <- rbind(tmp2, tmp[[i]])
    }
    return(tmp2)
  }

  ## providers
  provider.ind <- grep("Purchasable Catalog", headers)
  providers <- strsplit(unlist(strsplit(data[provider.ind], ";")), ":")

  tmp <- NULL
  for(i in 1:length(providers)) {
    tmp <- rbind(tmp, providers[[i]])
  }
  colnames(tmp) <- c("catalog", "code")
  providers <- as.data.frame(tmp, stringsAsFactors=FALSE)


  ## database annotations
  db.ind <- grep("Annotated Catalogs", headers)
  dbs <- strsplit(unlist(strsplit(data[db.ind], ";")), ":")

  tmp <- NULL
  for(i in 1:length(dbs)) {
    tmp <- rbind(tmp, dbs[[i]])
  }
  colnames(tmp) <- c("database", "code")
  dbs <- as.data.frame(tmp, stringsAsFactors=FALSE)
  
  
  data <- data[-c(provider.ind, db.ind)]
  headers <- headers[-c(provider.ind, db.ind)]
  names(data) <- headers
  data <- as.data.frame(data, stringsAsFactors=FALSE)
  
  out <- list(data=data, providers=providers, annotations=dbs)
  
  return(out)

}

  
